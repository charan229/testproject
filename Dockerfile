FROM python:3.5
RUN mkdir /code
WORKDIR /code
ADD . /code/
MAINTAINER : CHARAN

ENV : PYTHONUNBUFFERED 1
COPY  ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt
RUN : mkdir /app

WORKDIR : /app
 COPY  ./app /app


 USER root
